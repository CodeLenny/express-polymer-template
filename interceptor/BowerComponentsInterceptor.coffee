InterceptRule = require "fs-intercept/InterceptRule"
class BowerComponentsInterceptor extends InterceptRule

  intercept: (method, [path]) ->
    return no unless path and typeof path.indexOf is "function"
    path.indexOf("/elements/") > -1 and path.indexOf("/bower_components/") is -1

  readFile: (path, opts, cb) ->
    file = path.replace "/elements/", "/elements/bower_components/"
    require("fs").readFile file, "utf8", cb

module.exports = BowerComponentsInterceptor
