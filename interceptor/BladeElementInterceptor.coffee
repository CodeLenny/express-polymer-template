BladeInterceptor = require "./BladeInterceptor"

class BladeElementInterceptor extends BladeInterceptor

  opts: {
    importPath: "/elements/bower_components//"
  }

  intercept: (method, [path]) ->
    return no unless path and typeof path.indexOf is "function"
    path.indexOf("/elements/") > -1 and path.indexOf(".html") > -1

module.exports = BladeElementInterceptor
