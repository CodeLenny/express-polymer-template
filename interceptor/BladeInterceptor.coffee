InterceptRule = require "fs-intercept/InterceptRule"

tmp = require "tmp"
statBackup = require("fs").stat

class BladeInterceptor extends InterceptRule

  ###
  Options passed to the compiled template.
  ###
  opts: {}

  constructor: (dev=no, opts) ->
    @opts = Object.assign {}, @opts, opts, {dev: dev}

  intercept: (method, [path]) ->
    return no unless path and typeof path.indexOf is "function"
    path.indexOf(".html") > -1

  stat: (file, cb) ->
    tmp.file {discardDescriptor: yes}, (err, path, fd, cleanup) =>
      _err = (err) ->
        cleanup()
        cb err
      if err then return _err err
      @readFile file, "utf8", (err, html) ->
        if err then return _err err
        require("fs").writeFile path, html, (err) ->
          if err then return _err err
          statBackup path, (err, stats) ->
            if err then return _err err
            cleanup()
            cb null, stats

  readFile: (path, opts, cb) ->
    file = path.replace ".html", ".blade"
    require("fs").readFile file, "utf8", (err, blade) =>
      return cb err if err
      require("blade").compile blade, {
        filename: file
      }, (err, tmpl) =>
        return cb err if err
        tmpl @opts, cb

module.exports = BladeInterceptor
