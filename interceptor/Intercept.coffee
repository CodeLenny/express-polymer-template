###
Intercepts file reads.
###
class Intercept

  ###
  @property {FSReadInterceptor} the interceptor.
  ###
  interceptor: null

  ###
  @param {Boolean} dev `true` to set development mode in Blade templates.
  ###
  constructor: (dev=no) ->
    InterceptRule = require "fs-intercept/InterceptRule"
    FSReadInterceptor = require "fs-intercept"
    FSReadInterceptor::interceptedStat = (opts..., cb) ->
      if typeof cb isnt "function"
        deasync = require "deasync"
        return (deasync => @interceptedCall arguments...)("stat", arguments, {})
      @interceptedCall "stat", opts, {}, cb
    BladeElementInterceptor = require "./BladeElementInterceptor"
    BladeInterceptor = require "./BladeInterceptor"
    @interceptor = new FSReadInterceptor()
    @interceptor.use new BladeElementInterceptor dev, {}
    @interceptor.use new BladeInterceptor dev, {}

  ###
  Start intercepting file reads.
  @return {Intercept} `this` for chaining
  ###
  start: ->
    @interceptor.intercept()
    #require("fs").statSync = @interceptor.interceptedStat
    @

module.exports = Intercept
