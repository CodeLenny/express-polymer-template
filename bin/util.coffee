path = require "path"
Promise = require "bluebird"
fs = Promise.promisifyAll require "fs-extra"
PromiseBar = require "promise.bar"
pkg = require "../package.json"
{exec} = require "child-process-promise"
glob = require "glob-promise"
chalk = require "chalk"

root = path.resolve "#{__dirname}/.."

###
0 = silent
1 = very few lines of output - summary, errors, etc.
2 = default.  Test output, etc.
3 = Louder.  Describe steps, etc.
4 = Loud.  Install output, etc.
###
verbosity = 2

Promise.bar = -> PromiseBar.all arguments...

###
Finds all sub-packages that include a `package.json` file.
@return {Promise<Array>} resolves to a list of directories.
###
subPackages = ->
  Promise
    .resolve glob "#{root}/*/package.json"
    .map (manifest) ->
      path: path.resolve manifest, "../"
      name: path.basename path.dirname manifest

###
Runs `yarn install` on the given directory.
@param {String} dir the directory to install packages for
@return {Promise} resolves when the directory has been unpacked.
###
install = (dir) ->
  #exec "cd #{dir}; yarn install --no-lockfile"
  exec "cd #{dir}; npm install"
    .then (res) ->
      return res unless verbosity > 2
      console.log """
        Ran #{chalk.blue "npm install"} on #{chalk.dim.blue dir}.
      """
      return res unless verbosity > 3
      console.log "#{res.stdout}\n#{res.stderr}"
      res

testLogs = {}
###
Runs `npm test` on the given directory.
@param {String} dir the directory to run tests in.
@return {Promise} resolves when the directory has been tested.
###
test = (dir) ->
  exec "cd #{dir}; FORCE_COLOR=true npm test"
    .then (res) ->
      testLogs[dir] = res
      res
    .catch (err) ->
      testLogs[dir] = err
      testLogs[dir].failure = true
      err

###
Determines if a package has references to other packages in this repository.
@param {String} dir the path to a local package
@return {Boolean} `true` if the package has local references
###
hasLocalPackages = (dir) ->
  manifest = require "#{dir}/package.json"
  for own name, location of Object.assign {}, manifest.dependencies, manifest.devDependencies
    if location.indexOf("file:") > -1 then return yes
  no

###
Create symbolic links to other local packages in this repository.
@param {String} dir the path to a local package
@return {Promise} resolves when finished.
###
createSymlink = (dir) ->
  manifest = require "#{dir}/package.json"
  links = []
  for own name, location of Object.assign {}, manifest.dependencies, manifest.devDependencies
    continue unless location.indexOf("file:") > -1
    do (name, location) ->
      symlink = path.resolve dir, "node_modules", name
      target = path.resolve dir, location.replace "file:", ""
      task = fs
        .removeAsync symlink
        .catch (err) -> yes
        .then -> fs.ensureSymlinkAsync target, symlink
      if verbosity > 3 then console.log "Creating symlink #{symlink} to point at #{target}"
      links.push if verbosity > 2 then Promise.bar [task], {label: name} else task
  if verbosity > 1 then Promise.bar links, {label: manifest.name} else Promise.all links

###
Remove symbolic links to other local packages in this repository.
@param {String} dir the path to a local package
@return {Promise} resolves when finished.
###
unSymlink = (dir) ->
  manifest = require "#{dir}/package.json"
  links = []
  for own name, location of Object.assign {}, manifest.dependencies, manifest.devDependencies
    continue unless location.indexOf("file:") > -1
    do (name, location) ->
      symlink = path.resolve dir, "node_modules", name
      task = fs
        .removeAsync symlink
        .catch (err) -> yes
      if verbosity > 3 then console.log "Removing symlink #{symlink}"
      links.push if verbosity > 2 then Promise.bar [task], {label: name} else task
  if verbosity > 1 then Promise.bar links, {label: manifest.name} else Promise.all links

cli = require "commander"

cli.version pkg.version

cli.option "-l, --local", "Only operate on the current directory."
cli.option "--no-bar", "Disable Promise.bar."
cli.option "-s, --silent", "Produces no log output.", -> verbosity = 0
cli.option "-q, --quiet", "Produces minimal log output.", -> verbosity = 1
cli.option "-v, --verbose", "Increases log output level.  Repeatable.", -> verbosity++

enablePromiseBar = ->
  unless process.env.CI or cli.noBar or verbosity < 1 then PromiseBar.enable()

cli
  .command "install"
  .description "Install all dependencies for #{pkg.name}"
  .action (opts) ->
    if cli.local then return install process.cwd()
    enablePromiseBar()
    subPackages()
      .then (dirs) ->
        tasks = []
        for dir in dirs
          tasks.push Promise.bar [install dir.path], {label: dir.name}
        Promise.bar tasks, {label: "Packages"}
      .catch (err) ->
        if verbosity > 1 then console.trace err
        process.exit 1

cli
  .command "test"
  .description "Test all sub-packages in #{pkg.name}"
  .option "-d, --dirty", "Skip installing dependencies"
  .option "--no-side-server", "Disables running client-side tests that communicate with the actual server."
  .action (opts) ->
    if cli.local
      pre = if opts.dirty then Promise.resolve() else install(process.cwd())
      return pre
        .then -> test(process.cwd())
        .then -> if verbosity > 0 then console.log testLogs[process.cwd()].stdout
    enablePromiseBar()
    subPackages()
      .then (dirs) ->
        dirTests = []
        for dir in dirs
          do (dir) ->
            [tasks, sideServer] = [[], null]
            pre = if opts.dirty then Promise.resolve() else install dir.path
            tasks.push Promise.bar [pre], {label: "Install"}
            if opts.sideServer and dir.path is path.resolve "#{__dirname}/../elements"
              sideServer = true
              pre = pre.then ->
                ServerStarter = require "#{__dirname}/../elements/start-server/ServerStarter"
                sideServer = new ServerStarter()
                sideServer.start()
              tasks.push Promise.bar [pre], {label: "Start Server"}
            t = pre.then -> test dir.path
            if opts.dirty and not opts.sideServer
              return dirTests.push Promise.bar [t], {label: dir.name}
            tasks.push Promise.bar [t], {label: "Test"}
            if sideServer
              clean = t.then ->
                sideServer.stop()
              tasks.push Promise.bar [clean], {label: "Stop Server"}
            dirTests.push Promise.bar tasks, {label: dir.name}
        Promise.bar dirTests, {label: "Packages"}
      .then ->
        PromiseBar.end()
        for dir, log of testLogs
          if verbosity > 1 then console.log log.stdout
          if log.failure
            if verbosity > 0 then console.log chalk.red "Tests for #{chalk.blue dir} failed."
            process.exit 1
      .catch (err) ->
        if verbosity > 1 then console.trace err
        process.exit 1

cli
  .command "docs"
  .description "Generate documentation for the repository."
  .option "--polymer", "Bundles all documentation for Polymer elements into distributable HTML files"
  .option "--no-coffee", "Disables documenting CoffeeScript files"
  .action (opts) ->
    enablePromiseBar()
    fs
      .ensureDirAsync "#{root}/doc/"
      .then ->
        tasks = []

        if opts.polymer
          Intercept = require "#{root}/interceptor/Intercept"
          BowerComponentsInterceptor = require "#{root}/interceptor/BowerComponentsInterceptor"
          Vulcanize = require "vulcanize"
          interceptor = new Intercept()
          interceptor.interceptor.use new BowerComponentsInterceptor()
          interceptor.start()
          vulcan = Promise.promisifyAll new Vulcanize
            abspath: path.relative process.cwd(), path.resolve "#{root}/elements/"
            inlineScripts: yes
            inlineCss: yes
            stripComments: yes
          dir = fs.ensureDirAsync "#{root}/doc/elements/"
          index = dir
            .then -> vulcan.processAsync "index.html"
            .then (html) -> fs.writeFileAsync "#{root}/doc/elements/index.html", html
          if verbosity > 2 then index = Promise.bar [index], {label: "index.html"}
          elements = dir
            .then -> vulcan.processAsync "elements.html"
            .then (html) -> fs.writeFileAsync "#{root}/doc/elements/elements.html", html
          if verbosity > 2 then elements = Promise.bar [elements], {label: "elements.html"}
          poly = [index, elements]
          tasks.push if verbosity > 1 then Promise.bar poly, {label: "Polymer Docs"} else Promise.all poly

        if opts.coffee
          codo = exec "cd #{root}/; node_modules/.bin/codo"
          if verbosity > 1 then codo = Promise.bar [codo], {label: "Codo"}
          tasks.push codo

        if verbosity > 1 then Promise.bar tasks, {label: "Documentation"} else Promise.all tasks

cli
  .command "symlink"
  .alias "sym"
  .description "Create symbolic links for local dependencies."
  .action ->
    if cli.local then return createSymlink process.cwd()
    enablePromiseBar()
    subPackages()
      .filter (dir) -> hasLocalPackages dir.path
      .then (dirs) ->
        tasks = []
        for dir in dirs
          tasks.push createSymlink dir.path
        if verbosity > 1 then Promise.bar tasks, {label: "Create Symlinks"} else Promise.all tasks

cli
  .command "unsymlink"
  .alias "unsym"
  .description "Remove symbolic links created by 'symlink'."
  .action ->
    if cli.local then return unSymlink process.cwd()
    enablePromiseBar()
    subPackages()
      .filter (dir) -> hasLocalPackages dir.path
      .then (dirs) ->
        tasks = []
        for dir in dirs
          tasks.push unSymlink dir.path
        if verbosity > 1 then Promise.bar tasks, {label: "Remove Symlinks"} else Promise.all tasks

cli
  .command "start"
  .description "Start the webserver"
  .option "-c, --clean", "Makes sure all dependencies are up to date."
  .option "-p, --port [port]", "The port to listen to."
  .action (opts) ->
    pre = if opts.clean then install("#{root}/server/") else Promise.resolve()
    server = null
    pre
      .then ->
        Server = require "#{root}/server/Server"
        server = new Server()
        server.start opts.port
      .then -> server.port.promise
      .then (port) -> if verbosity > 0 then console.log "Listening on #{port}"

cli.parse process.argv
