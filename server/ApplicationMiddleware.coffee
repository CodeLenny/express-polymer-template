Promise = require "bluebird"
Promise.defer = require "promise-defer"
fs = Promise.promisifyAll require "fs"
express = require "express"
blade = require "blade"

###
Serves an HTML entrypoint for the Polymer application.
###
class ApplicationMiddleware

  ###
  Express middleware that serves the Polymer application.
  @return {express.Router}
  ###
  @middleware = ->
    router = express.Router()
    if process.env.NODE_ENV is "production"
      index = fs.readFileAsync "#{__dirname}/index.html", "utf8"
      router.get "/", (req, res) ->
        index.then (html) -> res.send html
    else
      router.get "/", (req, res) ->
        blade.renderFile "#{__dirname}/index.blade", {dev: yes}, (err, html) ->
          if err
            res.status 500
            return res.send err
          res.send html
    router

module.exports = ApplicationMiddleware
