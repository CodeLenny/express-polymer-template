Promise = require "bluebird"
Promise.defer = require "promise-defer"
express = require "express"
enableDestroy = require "server-destroy"
Intercept = require "interceptor/Intercept"
ApplicationMiddleware = require "./ApplicationMiddleware"
ServeElements = require "./ServeElements"
MessageBoardStore = require "message-board-store/MessageBoardStore"

###
The entrypoint to the application.
###
class Server

  ###
  @property {Deferred<http.Server>} the webserver for the application.
    Resolves once the server has started listening to HTTP co
  ###
  server: null

  ###
  @property {Deferred<Number>} the port used by the web server.
  ###
  port: null

  ###
  @property {MessageBoardStore} stores entries for a guestbook.
  ###
  store: null

  constructor: ->
    @server = Promise.defer()
    @port = Promise.defer()
    @app = express()
    unless process.env.NODE_ENV is "production"
      @interceptor = new Intercept().start()
      @app.use (req, res, next) ->
        res.header "Access-Control-Allow-Origin", "*"
        res.header "Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept"
        next()
      @app.use "/elements", ServeElements.router()
    @app.use "/app", ApplicationMiddleware.middleware()
    @store = new MessageBoardStore()
    @app.use "/messages", @store.router

  ###
  Starts the server listening.
  @param {Number} port See {Server#startHTTP}.
  @return {Promise} Resolves when the server is listening.
  ###
  start: (port) ->
    Promise.all [
      @startHTTP port
    ]

  ###
  Starts an HTTP server listening.
  @param {Number} port **Optional** the port to listen to.  Defaults to an automatically chosen unused port.
  @return {Promise<http.Server>} resolves when the server has started and is listening to web traffic.
  ###
  startHTTP: (port) ->
    server = @app.listen port, =>
      @port.resolve server.address().port
      enableDestroy server
      @server.resolve server
    @server.promise

  ###
  Stops the server.
  @return {Promise} resolves when the server has been terminated.
  ###
  stop: ->
    @server.promise.then (server) ->
      new Promise (resolve, reject) ->
        server.destroy -> resolve()

module.exports = Server
