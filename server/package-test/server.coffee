server = if process.env.CI then "docker" else "localhost"
url = process.env.URL ? "http://#{server}:#{process.env.PORT ? 80}/"

module.exports = {url}
