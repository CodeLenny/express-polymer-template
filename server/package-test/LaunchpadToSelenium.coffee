launchpad = require "./launchpad"

###
Provides Selenium {Capabilities} objects given Launchpad browsers.
###
class LaunchpadToSelenium

  ###
  Provides the capabilities of a Chrome browser.
  @param {launchpad.Browser}
  @return {Object, Capabilities}
  ###
  @chrome = (browser) ->
    browserName: "chrome"
    version: browser.version.match(/\d+/)[0]
    chromeOptions:
      binary: browser.binPath
      args: ["start-maximized"]

  @canary = @chrome

  ###
  Provides the capabilities of a Firefox browser.
  @param {launchpad.Browser}
  @return {Object, Capabilities}
  ###
  @firefox = (browser) ->
    version = parseInt browser.version.match(/\d+/)[0], 10
    marionette = version >= 47
    {
      browserName: "firefox"
      version: version
      firefox_binary: browser.binPath
      marionette: marionette
    }

  @aurora = @firefox

  ###
  Provides the capabilities of an Internet Explorer browser.
  @param {launchpad.Browser}
  @return {Object, Capabilities}
  ###
  @ie = (browser) ->
    browserName: "internet explorer"
    version: browser.version

  ###
  Provides the capabilities of a Safari browser.
  @param {launchpad.Browser}
  @return {Object, Capabilities}
  ###
  @safari = (browser) ->
    browserName: "safari"
    version: browser.version
    "safari.options":
      skipExtensionInstallation: yes

  ###
  Convert an array of launchpad browser details (e.g. from `launchpad.browsers()`) into an array of Selenium
  capabilities.
  @param {Array<launchpad.Browser>} browsers the Launchpad details of browsers.
  @return {Array<Object, Capabilities>} the capabilities for each browser given.
  ###
  @expand = (browsers) =>
    converter browser for browser in browsers when browser.name? and converter = @[browser.name]

  @_detected = null

  ###
  Get the capabilities of currently installed browsers.  Caches the installed browsers unless `cache` is overridden.
  @param {Boolean} cache If `false`, the system will be always be queried for installed browsers.  Defaults to `true`.
  @return {Array<Object, Capabilities>} the capabilities for each browser installed on the system.
  ###
  @detect = (cache=yes) ->
    if @_detected and cache then return @_detected
    @_detected = launchpad
      .localAsync()
      .then (launcher) -> launcher.browsersAsync()
      .then @expand

module.exports = LaunchpadToSelenium
