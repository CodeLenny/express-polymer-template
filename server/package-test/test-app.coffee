chai = require "chai"
chai.use chaiAsPromised = require "chai-as-promised"
should = chai.should()
server = require "./server"
webdriverio = require "webdriverio"
LaunchpadToSelenium = require "./LaunchpadToSelenium"

describe "/app/", ->

  @timeout 10 * 1000

  [client] = []

  before ->
    LaunchpadToSelenium
      .detect()
      .then (browsers) ->
        console.log "Detected #{browsers.length} browsers."
        browsers[0]
      .then (capabilities) -> client = webdriverio.remote { desiredCapabilities: capabilities }
      .then -> chaiAsPromised.transferPromiseness = client.transferPromiseness
      .then ->
        console.log "Starting the Selenium client."
        client.init()

  beforeEach ->
    client.url "#{server.url}app/"

  after ->
    client.end()

  it "should have <entry-point>", ->
    client
      .getTagName "entry-point"
      .then (name) -> name.toLowerCase().should.equal "entry-point"

  it "should only have 1 <entry-point>", ->
    client
      .$$ "entry-point"
      .then (els) -> els.length.should.equal 1
