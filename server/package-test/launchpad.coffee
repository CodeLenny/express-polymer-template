Promise = require "bluebird"
module.exports = launchpad = Promise.promisifyAll require "launchpad"

launcher = null

launchpad.localAsync = ->
  if launcher then return launcher
  launcher = Promise.promisify(launchpad.local)().then (local) -> Promise.promisifyAll local
