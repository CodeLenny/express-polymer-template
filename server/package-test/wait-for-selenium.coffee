Promise = require "bluebird"
request = Promise.promisifyAll require "request"
{exec} = require "child_process"

# If not started after `slow`, display the "forever" log.
slow = 20 * 1000

# Timeout of 1 minute
timeout = 5 * 60 * 1000

start = Date.now()

isSlow = -> (Date.now() - start) > slow

isDead = -> (Date.now() - start) > timeout

port = process.env.PORT ? 80
serverURL = if process.env.CI then "http://docker:#{port}/" else "http://localhost:#{port}/"

waitForServer = ->
  logSlow = yes
  reqServer = ->
    request.getAsync serverURL
      .catch (err) ->
        if isSlow() and logSlow
          logSlow = no
          exec "docker ps -l -q", (err, id) ->
            exec "docker logs #{id}", (err, stdout, stderr) ->
              console.log "Server is taking a while to start.  Docker log files:"
              console.log stdout
              console.log stderr
        if isDead()
          console.error "[ERROR]: Server has not been started in #{timeout / (60 * 1000)} minutes.  Aborting."
          console.error err.stack || err
          throw err
        Promise.delay(50).then reqServer
  reqServer()

waitForSelenium = ->
  logSlow = yes
  reqServer = ->
    request
      .getAsync "http://localhost:4444/wd/hub"
      .catch (err) ->
        if isSlow() and logSlow
          logSlow = no
          exec "#{__dirname}/../node_modules/.bin/forever logs 0", (err, stdout, stderr) ->
            console.log "Selenium is takinga while to start.  Selenium log files:"
            console.log stdout
            console.log stderr
        if isDead()
          console.error "[ERROR]: Selenium has not started in #{timeout / (60 * 1000)} minutes.  Aborting."
          console.error err.stack || err
          throw err
        Promise.delay(50).then reqServer
  reqServer()

waitForServers = ->
  Promise
    .all [
      waitForServer()
      waitForSelenium()
    ]
    .then ->
      console.log "Servers started."
    .catch (err) ->
      console.log "Servers failed to start.  Aborting."
      process.exit 1

module.exports = waitForServers()

if require.main is module then waitForSelenium()
