cli = require "commander"
pkg = require "#{__dirname}/../package.json"
Server = require "#{__dirname}/../Server"

cli.version pkg.version

cli.option "-p, --port [port]", "The HTTP port to run on.  Defaults to 80.", 80

cli.parse process.argv

server = new Server()
console.info "Attempting to start server on port #{cli.port}"
server.start cli.port
  .then ->
    console.info "Server has started."
