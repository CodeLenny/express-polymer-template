Promise = require "bluebird"
fs = Promise.promisifyAll require "fs"
path = require "path"
server = path.resolve "#{__dirname}/../"

cli = require "commander"

cli.option "--no-index", "Don't build the index."

cli.parse process.argv

if cli.index
  Intercept = require "interceptor/Intercept"
  Vulcanize = require "vulcanize"
  interceptor = new Intercept()
  interceptor.start()
  vulcan = Promise.promisifyAll new Vulcanize
    abspath: path.relative process.cwd(), server
    inlineScripts: yes
    inlineCss: yes
    stripComments: yes
  fs
    .unlinkAsync "#{server}/index.html"
    .catch (err) -> yes
    .then -> vulcan.processAsync "index.html"
    .then (html) -> fs.writeFileAsync "#{server}/index.html", html
    .catch (err) ->
      console.trace err
      process.exit 1
