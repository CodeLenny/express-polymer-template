express = require "express"
path = require "path"

###
Serves elements from the `elements` directory as well as the `bower_components` directory.

Only for testing.  For production, all dependencies should be vulcanized into the main file.
###
class ServeElements

  @router = ->
    router = express.Router()
    router.use "/elements", express.static path.resolve "#{__dirname}/../elements"
    router.use express.static path.resolve "#{__dirname}/../elements/bower_components"

module.exports = ServeElements
