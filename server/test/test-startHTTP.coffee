chai = require "chai"
should = chai.should()
Server = require "../Server"

describe "Server#startHTTP", ->

  it "starts an HTTP server", ->
    server = new Server()
    server.start()
      .then ->
        server.port.promise
      .then (port) ->
        port.should.be.a.number
      .then ->
        server.stop()
