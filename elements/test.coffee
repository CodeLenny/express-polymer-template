###
Tests elements via web-component-tester, but uses `interceptor` to dynamically compile files.
Modeled after `web-component-tester/bin/wct`.
###

Intercept = require "interceptor/Intercept"
interceptor = new Intercept().start()

wct = require "web-component-tester"
run = wct.cli.run process.env, process.argv.slice(2), process.stdout, (err) ->
  process.exit if err then 1 else 0

if run then run.then (-> process.exit 0), (-> process.exit 1)
