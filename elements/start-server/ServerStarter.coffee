express = require "express"
Server = require "#{__dirname}/../../server/Server"

###
Provides a web-server that allows creating and stoping {Server} instances.

Operates on port `5055`.
###
class ServerStarter

  ###
  @property {Number} the HTTP port to listen to.
  ###
  @PORT = 5055

  ###
  @property {Array<Server>} stores active servers.
  ###
  servers: null

  constructor: ->
    @servers = []
    @app = express()
    @app.use (req, res, next) ->
      res.header "Access-Control-Allow-Origin", "*"
      res.header "Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept"
      if process.env.NO_SIDE_SERVER
        res.status 503
        return res.send ""
      next()
    @app.get "/", (req, res) =>
      res.status 200
      res.send ""
    @app.get "/start/", (req, res) =>
      server = new Server()
      server
        .start()
        .then -> server.port.promise
        .then (port) =>
          console.log "Started server #{port}"
          @servers[port] = server
          res.send "#{port}"
        .catch (err) ->
          console.error "Error while starting new Server instance"
          console.trace err
          res.status 500
          res.send ""
    @app.get "/stop/:id/", (req, res) =>
      server = @servers[req.params.id]
      unless server
        console.error "Attempted to stop unknown Server instance (#{req.params.id})"
        res.status 400
        return res.send "ERROR"
      console.log "Stopping server #{req.params.id}"
      server.stop().then =>
        delete @servers[req.params.id]
        res.send "OK"

  start: -> @_server = @app.listen @constructor.PORT

  stop: -> @_server.close()

module.exports = ServerStarter
