Promise = require "bluebird"
express = require "express"
bodyParser = require "body-parser"
customError = require "custom-error"

InvalidTitle = customError "InvalidTitle"
InvalidMessage = customError "InvalidMessage"

###
Stores messages for a "message board" - a public guest book.
###
class MessageBoardStore

  ###
  @property {express.Router} a router that stores new messages and serves stored messages.
  ###
  router: null

  ###
  @property {Array<Object>} stores messages as `{id, title, message}`
  ###
  messages: null

  ###
  @property {Number} a counter to create unique IDs.
  ###
  lastID: 0

  constructor: ->
    @messages = []
    @router = express.Router()
    @router.get "/latest/", (req, res) =>
      res.json @messages
    @router.post "/submit/", bodyParser.json(), (req, res) =>
      {title, text} = req.body
      Promise
        .resolve @validate {title, text}
        .then =>
          id = ++@lastID
          @messages.push {id, title, text}
          res.send "Message added."
        .catch InvalidTitle, (err) ->
          res.status 400
          res.send "Invalid title field."
        .catch InvalidMessage, (err) ->
          res.status 400
          res.send "Invalid message field."

  ###
  Validates a given post.
  @param {String} title a title for the post
  @param {String} text the message body to include in the post
  @return {Promise} rejects if any errors occur.  Resolves if message passes validation.
  @throws {InvalidTitle} rejects if the title is unacceptable
  @throws {InvalidMessage} rejects if the message body is unacceptable
  ###
  validate: ({title, text}) ->
    unless title and typeof title is "string" then return Promise.reject new InvalidTitle "Title must be a string."
    if title.length > 80 then return Promise.reject new InvalidTitle "Title is too long."
    if title.length < 1 then return Promise.reject new InvalidTitle "Title is required."
    unless text and typeof text is "string" then return Promise.reject new InvalidMessage "Message must be a string."
    if text.length > 500 then return Promise.reject new InvalidMessage "Message is too long."
    if text.length < 1 then return Promise.reject new InvalidMessage "Message is required."
    Promise.resolve()

module.exports = MessageBoardStore
