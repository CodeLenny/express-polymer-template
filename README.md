# Express/Polymer App

This repository is a template for a monolithic [Node][], [Express][], and [Polymer][] application, written in
[CoffeeScript][] and [Blade][].
See the [wiki](https://gitlab.com/CodeLenny/express-polymer-template/wikis/home) for template documentation.

## Development

### Repository Setup

Clone the repository via Git.

A utility script, `/bin/util` manages bulk installation and testing of packages inside this repository.
For convenience, you can add the script to your `PATH` when developing, so you can use `util` without specifying a path.

```bash
git clone <REPO>
cd <REPO>
source bin/profile # Optional

bin/util install   # or `util install` if you've sourced bin/profile
bin/util sym       # or `util sym`
```

`util install` installs the dependencies for all packages inside the repository, and `util sym` creates symbolic links
between internal libraries, so changes take effect immediately instead of having to reinstall internal libraries.

See `util --help` for more information about the different utility commands that can be run.

### Repository Organization

#### Server Entrypoint

`/server` is an HTTP "entrypoint", the externally facing HTTP server.  Inside, different libraries are pulled together
to create all server-side functionality that the client expects.

In addition, `/server/ServeElements.coffee` serves the Polymer elements inside the `/elements` directory during
development.

#### Server-Side Libraries

Server-side tasks are carried out by libraries.

- `/interceptor` handles runtime compilation of resources during development, testing, and packaging.
- `/message-board-store` stores and serves messages for the client.

Each server-side library contains an NPM manifest (`package.json`) and can install other local libraries.
Libraries are individually tested by running the `npm test` script inside the manifest.

#### Client-Side Polymer Elements

Most client-side functionality is wrapped into Polymer elements, which are stored in `/elements`.

Elements are served by `/server/ServeElements.coffee` during development, and fetch dependencies via HTML imports.
When building for production, [polymer-bundler][] is used to concatenate all dependencies into a single HTML file.

Polymer elements are tested using [web-component-tester][], running tests for all elements in browser instances.

### Testing Strategy

This repository uses four types of testing.

- **Server-Side Unit Tests**
  Server-side libraries should use extensive unit-tests that cover classes, methods, and functions, and cover most
  edge-cases.  These can be directly run via [Mocha][] and [Chai][], or similar platforms.
- **Server-Side Integration/API Tests**
  Server-side libraries and entrypoints that define an HTTP, WebSocket or JavaScript API should provide end-to-end
  testing using API methods.  Tests can be run directly through [Mocha][] and [Chai][] (see [chai-http][]), normally in
  the same Mocha run as unit tests.
- **Element Tests**
  Polymer elements should be individually tested through [web-component-tester][], examining methods, input/output
  variables, and basic uses of the elements.
  For elements that communicate over the network to transmit or receive data, tests should be run both on mocked servers
  via [Sinon][] (or a similar library) as well as on actual instances of the server.
  These tests should be called through [web-component-tester][], with an external wrapper that can create and tear down
  server instances as needed.
- **Package Testing**
  Entrypoints are often published as Docker images, NPM packages, or other methods.  Once build scripts have been run
  and a bundle has been created, the package should be installed and started, and additional testing run to ensure that
  the libraries and elements expressed in the unit and integration tests have been included and are running in the
  bundled image.
  Tests should be run via [chai-http][] to check HTTP API responses, and raw [Selenium][selenium-webdriver] tests to
  check website contents and elements.

[Node]: https://nodejs.org/
[Express]: http://expressjs.com/
[Polymer]: https://www.polymer-project.org/
[CoffeeScript]: http://coffeescript.org/
[Blade]: https://github.com/bminer/node-blade
[Mocha]: https://mochajs.org/
[Chai]: http://chaijs.com/
[chai-http]: https://github.com/chaijs/chai-http
[web-component-tester]: https://github.com/Polymer/web-component-tester
[polymer-bundler]: https://github.com/Polymer/polymer-bundler
[Sinon]: http://sinonjs.org/
[selenium-webdriver]: https://www.npmjs.com/package/selenium-webdriver
